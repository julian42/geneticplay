import copy
import random
import datetime
import sys
sys.setrecursionlimit(10000) # 10000 is an example, try with different values
random.seed(14)



starttime = datetime.datetime.now()

def write2file(s):
	f = open("output.txt","a")
	f.write(s + "\n")
	f.close()




class Node():
	A = None
	B = None
	def __init__():
		pass
		#print "Node"


	def growRandom(s):
		leaf_types = [Constant, Input]
		node_types = [Add, Subtract, Multiply, Modulus, Divide]
		available_types = leaf_types*3+node_types
		r = random.randint(0,len(available_types)-1)
		#print available_types[r]
		return available_types[r]()

	def reproduce(s):
		r = random.randint(0,99)
		#1% mutations
		if r < 21:
			#print "randomize leg completely"
			return s.growRandom()
		else:
			r = copy.deepcopy(s)
			if s.A != None:
				r.A=s.A.reproduce()
			if s.B != None:
				r.B = s.B.reproduce()
			return r

	def describe(s):
		return ""

	def cost(s):
		return 1

	def evaluate(s, v):
		return 1

	def eval_list(s, g):
		r = []
		for i in range(len(g)):
			r.append(s.evaluate(i))
		return r

	def list_error(s,g):
		r = []
		for i in range(len(g)):
			r.append(abs(g[i]-s.evaluate(i)))
		return r

	def error(s, g):
		return sum(s.list_error(g))

	def error_pen(s, g):
		return s.error(g) + max((s.cost()-30)/10,0)

class Constant(Node):
	def __init__(s):
		s.value = random.randint(1,16)
		#print "constant %d" % (s.value)
	def evaluate(s, v):
		return s.value
	def describe(s):
		return str(s.value)

class Input(Node):
	def __init__(s):
		pass
	def evaluate(s, v):
		return v
	def describe(s):
		return "X"

class Add(Node):
	def __init__(s):
		s.A = s.growRandom()
		s.B = s.growRandom()
	def evaluate(s, v):
		return s.A.evaluate(v) + s.B.evaluate(v)
	def describe(s):
		return "(%s+%s)" % (s.A.describe(), s.B.describe())
	def cost(s):
		return 1 + s.A.cost() + s.B.cost()

class Subtract(Node):
	def __init__(s):
		s.A = s.growRandom()
		s.B = s.growRandom()
	def evaluate(s, v):
		return s.A.evaluate(v) - s.B.evaluate(v)
	def describe(s):
		return "(%s-%s)" % (s.A.describe(), s.B.describe())
	def cost(s):
		return 1 + s.A.cost() + s.B.cost()

class Multiply(Node):
	def __init__(s):
		s.A = s.growRandom()
		s.B = s.growRandom()
	def evaluate(s, v):
		return s.A.evaluate(v) * s.B.evaluate(v)
	def describe(s):
		return "(%s*%s)" % (s.A.describe(), s.B.describe())
	def cost(s):
		return 1 + s.A.cost() + s.B.cost()

class Modulus(Node):
	def __init__(s):
		s.A = s.growRandom()
		s.B = s.growRandom()
	def evaluate(s, v):
		b = s.B.evaluate(v)
		if b==0:
			return 16 #the largest number we know
		return s.A.evaluate(v) % b
	def describe(s):
		return "(%s%%%s)" % (s.A.describe(), s.B.describe())
	def cost(s):
		return 1 + s.A.cost() + s.B.cost()

class Divide(Node):
	def __init__(s):
		s.A = s.growRandom()
		s.B = s.growRandom()
	def evaluate(s, v):
		b = s.B.evaluate(v)
		if b==0:
			return random.randint(1,99)
		return s.A.evaluate(v) / b
	def describe(s):
		return "(%s/%s)" % (s.A.describe(), s.B.describe())
	def cost(s):
		return 1 + s.A.cost() + s.B.cost()



class Island():
	def __init__(s, n):
		s.name = n
		s.goal = [1,1]
		s.N = 12000
		s.n = 300
		s.population = []
		for i in range(s.N):
			s.population.append(Constant().reproduce())
		s.generation = 1
		s.best = None

	def dump_inhabitant(s, i):
		s.population.append(i)
	def evolve(s):
		s.population.sort(key=lambda x: x.error_pen(s.goal))
		
		e = s.population[0].error(s.goal)
		if e == 0: #s.best == None or e < s.best:
			lines = []
			lines.append(s.name+" "+str(datetime.datetime.now() - starttime))
			lines.append("Gen:%d   Pop:%d   Err:%0.1f   Cost:%d" % (s.generation, s.N, e, s.population[0].cost()))
			lines.append(s.population[0].describe())
			lines.append(str(s.goal))
			lines.append(str(s.population[0].eval_list(s.goal)))
			lines.append("")
			
			for l in lines:
				write2file(l)
				print l
				
			s.best = e 
			
			if e == 0: # len(goal)/4:
				s.best = None
				if s.name == "Amager":
					s.goal.append(s.goal[-2]+s.goal[-1])
				else:
					a = 0
					if len(s.goal) > 2:
						a = s.goal[-3]
					s.goal.append(a + s.goal[-2]+s.goal[-1])
		s.generation += 1
		s.population = s.population[0:s.n]
		while len(s.population) < s.N:
			nw = s.population[random.randint(0,len(s.population)-1)].reproduce()
			s.population.append(nw)
	
	def getRandomNode(s):
		return s.population[random.randint(0,len(s.population)-1)]


islands = [] 
islands.append(Island("Amager"))
islands.append(Island("Sjaelland"))





g = 0
while True:

	for i in islands:
		i.evolve()

	if g % 5 == 0:
		for i in islands:
			for j in islands:
				i.dump_inhabitant(j.getRandomNode())


	g += 1


